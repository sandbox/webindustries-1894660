<?php

/**
 * @file
 * Customize the e-mails sent by Basecamp.
 *
 * Available variables:
 * - $todolist: Todolist of unassigned todos.
 */
?>
<div id='basecamp-mail'>
  <div class = 'basecamp-mail-body'>
    <div class='basecamp-greeting'>
      <p>Hello,</p>
    </div>
    <div class='basecamp-message'>
      <p>This is the list of unassinged todos from Basecamp</p>
      <div id='basecamp-mail-todolist'>
        <div class='basecamp-todolist-title'>
          <h2 class='basecamp-todolist-title'>Unassigned todos</h2>
        </div>
        <div class='basecamp-todolist'>
          <?php print $todolist; ?>
        </div>
        <div class='basecamp-end-greeting'>
          <p>Regards,<p>
          <p>Team Basecamp</p>
        </div>
      </div>
    </div>
  </div>
</div>
