<?php

/**
 * @file
 * Page callbacks for Basecamp Integration module.
 */

/**
 * Configuration form of Basecamp.
 */
function basecamp_config_form(&$form_state) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  
  $form['basecamp_uid'] = array(
    '#type' => 'textfield',
    '#title' => 'Basecamp id',
    '#description' => t('Please enter the basecamp id. Do not include trailing slash.'),
    '#required' => TRUE,
    '#size' => 15,
    '#element_validate' => array('number'),
    '#default_value' => $basecamp_config['uid'],
    '#field_prefix' => 'https://www.basecamp.com/',
  );
  $form['basecamp_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Basecamp Username',
    '#description' => t('Please enter your Basecamp username.'),
    '#required' => TRUE,
    '#size' => 15,
    '#default_value' => $basecamp_config['username'],
  );
  $form['basecamp_password'] = array(
    '#type' => 'password',
    '#title' => 'Basecamp Password',
    '#description' => t('Please enter your Basecamp password.'),
    '#required' => TRUE,
    '#size' => 15,
    '#post_render' => array('basecamp_set_password'),
  );
  $form['basecamp_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#description' => t('Please enter email address. Unsigned Todos list will be sent to this address.'),
    '#required' => TRUE,
    '#default_value' => $basecamp_config['email'],
  );
  $form['basecamp_time_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Time'),
    '#field_suffix' => 'Hours',
    '#description' => t('Time interval to trigger email delivery.'),
    '#size' => 5,
    '#default_value' => $basecamp_config['time_interval'],
  );
  if ($basecamp_config['uid'] != '') {
    $projects = _basecamp_get_projects();
    $basecamp_projects = array();
    if (!empty($projects)) {
      foreach ($projects as $project) {
        $basecamp_projects[$project->id] = $project->name;
      }
      $default_projects = variable_get('basecamp_selected_projects', array());
      $form['basecamp_project_list'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Available projects',
        '#options' => $basecamp_projects,
        '#description' => t('Select the projects.'),
        '#default_value' => $default_projects,
      );
    }
    else {
      drupal_set_message(t('It seems like you do not have projects in your basecamp account.'), 'warning');
    }
  }
  else {
    drupal_set_message(t('You need to configure basecamp before selecting from the available projects.'), 'warning');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#validate'][] = 'basecamp_config_form_validate';
  return $form;
}

/**
 * Sets the default value for password.
 */
function basecamp_set_password(&$form) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $basecamp_password = $basecamp_config['password'];
  $form = str_replace('input type="password"', 'input type="password" value="' . $basecamp_password . '"', $form);
  return $form;
}

/**
 * Validater for Basecamp Configuration form.
 */
function basecamp_config_form_validate($form, &$form_state) {
  $basecamp_email = $form_state['values']['basecamp_email'];
  if (!valid_email_address($basecamp_email)) {
    form_set_error('basecamp_email', t('Please enter valid email address.'));
  }
  $basecamp_time_interval = $form_state['values']['basecamp_time_interval'];
  if (!is_numeric($basecamp_time_interval)) {
    form_set_error('basecamp_time_interval', t('Please enter appropriate numeric value for hours.'));
  }
}

/**
 * Submit handler for Basecamp Configuration form.
 */
function basecamp_config_form_submit($form, &$form_state) {
  $basecamp_uid = $form_state['values']['basecamp_uid'];
  $basecamp_username = $form_state['values']['basecamp_username'];
  $basecamp_password = $form_state['values']['basecamp_password'];
  $basecamp_email = $form_state['values']['basecamp_email'];
  $basecamp_time_interval = $form_state['values']['basecamp_time_interval'];

  $basecamp_config = array(
    'uid' => $basecamp_uid,
    'username' => $basecamp_username,
    'password' => $basecamp_password,
    'email' => $basecamp_email,
    'time_interval' => (int)$basecamp_time_interval,
  );
  $basecamp_selected_projects = $form_state['values']['basecamp_project_list'];

  variable_set('basecamp_configuration', $basecamp_config);
  variable_set('basecamp_selected_projects', $basecamp_selected_projects);

  $form_state['rebuild'] = TRUE;
  $form_state['redirect'] = 'admin/structure/basecamp';
  drupal_set_message(t('Basecamp configured successfully.'), 'status');
}

/**
 * Lists the basecamp projects.
 */
function basecamp_project_list() {
  $output = '';
  $projects = _basecamp_get_projects();
  if ($projects) {
    $projects_by_id = array();
    $accesses = array();

    foreach ($projects as $project) {
      $projects_by_id[] = _basecamp_get_project_by_id($project->id);
    }

    $table_rows = array();
    $header = array(t('Project'), t('Creator name'), t('Creator\'s email address'), t('Team mates'));

    $attributes = array(
      'height' => '40px',
      'width' => '40px',
      'style' => 'margin-left:8px;',
    );

    foreach ($projects_by_id as $project) {
      $row = array();
      $project_name = $project->name;
      $project_creator = $project->creator->name;
      $creator = _basecamp_get_person($project->creator->id);
      $creator_email = $creator->email_address;
      $row[] = $project_name;
      $row[] = $project_creator;
      $row[] = $creator_email;
      $accesses = _basecamp_get_accesses($project->id);
      $avtar_column = '';
      foreach ($accesses as $access) {
        $avtar_url = $access->avatar_url;
        $avtar_column .= theme_image($avtar_url, $access->name, $access->name, $attributes, FALSE);
      }
      $row[] = $avtar_column;
      $table_rows[] = array('data' => $row);
    }
    $output .= theme('table', $header, $table_rows);
  }
  else {
    drupal_set_message(t('It seems like there are no projects in your basecamp account.'), 'warning');
  }
  return $output;
}

/**
 * Todo list with filters.
 */
function basecamp_list(&$form_state) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $url = $basecamp_config['url'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];

  $disabled_flag = FALSE;

  $selected_project_names = array();
  $selected_projects_id = _basecamp_selected_projects();
  $selected_project_names = _basecamp_get_selected_projects_names();
  if (!$selected_project_names) {
    drupal_set_message(t('It seems like there are no projects selected. Please select projects on ') . l(t('Configuration'), 'admin/structure/basecamp/config') . t(' page.'), 'warning');
    $disabled_flag = TRUE;
    unset($_SESSION['basecamp_project_name']);
    unset($_SESSION['basecamp_todo_type']);
  }
  else {
    $selected_project_names = _basecamp_get_selected_projects_names();
  }
  $selected_projects = array();
  if (!empty($selected_projects_id)) {
    $selected_projects = array_combine($selected_projects_id, $selected_project_names);
  }
  $todo_type_options = array(0 => 'ALL', 1 => 'UNASSIGNED');
  $form['project_list_filter'] = array(
    '#type' => 'select',
    '#title' => t('Select project'),
    '#options' => $selected_projects,
    '#disabled' => $disabled_flag,
    '#default_value' => isset($_SESSION['basecamp_project_name']) ? $selected_projects[$_SESSION['basecamp_project_name']] : reset($selected_projects),
  );
  $form['todo_list_filter'] = array(
    '#type' => 'select',
    '#title' => t('Todo type'),
    '#options' => $todo_type_options,
    '#disabled' => $disabled_flag,
    '#default_value' => isset($_SESSION['basecamp_todo_type']) ? $_SESSION['basecamp_todo_type'] : $todo_type_options[0],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Show'),
  );
  return $form;
}

/**
 * Theme function for Todo listing.
 */
function theme_basecamp_list($form) {
  $output = '';
  $basecamp_config = variable_get('basecamp_configuration', array());
  $header = array('Todo List');
  $is_configured = TRUE;
  if ($basecamp_config['uid'] == '') {
    $is_configured = FALSE;
    $output .= '<p>' . t('Basecamp is not configured yet. ') . l(t('Configure'), 'admin/structure/basecamp/config');
    return $output;
  }
  else {
    $output .= drupal_render($form);
  }

  if (isset($_SESSION['basecamp_project_name'])) {
    $project_id = $_SESSION['basecamp_project_name'];
  }
  else {
    $project_name = $form['project_list_filter']['#value'];
    $project_id = array_search($project_name, $form['project_list_filter']['#options']);
  }

  if (isset($_SESSION['basecamp_todo_type'])) {
    $todo_type_index = $_SESSION['basecamp_todo_type'];
    $todo_type = $form['todo_list_filter']['#options'][$todo_type_index];
  }
  else {
    $todo_type = $form['todo_list_filter']['#value'];
  }

  $todolist_ids = _basecamp_todolist_ids($project_id);
  $todos = array();

  foreach ($todolist_ids as $todolist_id) {
    $todos[] = _basecamp_get_todos($project_id, $todolist_id, $todo_type);
  }

  $table_rows = array();

  foreach ($todos as $todo_item) {
    foreach ($todo_item as $todo_content) {
      $table_rows[] = array('data' => array($todo_content));
    }
  }

  $output .= theme('table', $header, $table_rows);
  return $output;
}

/**
 * Submit handler for Todo listing.
 */
function basecamp_list_submit($form, &$form_state) {
  $project_name = $form_state['values']['project_list_filter'];
  $todo_type = $form_state['values']['todo_list_filter'];
  $form_state['storage']['project_list_filter'] = $project_name;
  $form_state['storage']['todo_list_filter'] = $todo_type;
  $_SESSION['basecamp_project_name'] = (int)$project_name;
  $_SESSION['basecamp_todo_type'] = (int)$todo_type;
  $form_state['rebuild'] = TRUE;
  $form_state['redirect'] = 'admin/structure/basecamp/list';
}

/**
 * Helper functions for Basecamp Integration module.
 */

/**
 * Creates options to be passed with HTTP request.
 * @return array containing list of options.
 */
function _basecamp_http_options() {
  $options = array(
    'headers' => array(
      'Accept' => 'application/json',
    ),
    'method' => 'GET',
    'data' => '',
  );
  return $options;
}

/**
 * Get list of projects from Basecamp account.
 * @return array containing list of projects.
 */
function _basecamp_get_projects() {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $uid = $basecamp_config['uid'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects.json';
  $options = _basecamp_http_options();
  $projects = array();
  $projects_json = drupal_http_request($basecamp_url, $options);
  $projects = json_decode($projects_json->data);
  if (empty($projects)) {
    return FALSE;
  }
  return $projects;
}

/**
 * Provides single project details.
 * @param $project_id id of the project.
 * @return object containing details of projects.
 */
function _basecamp_get_project_by_id($project_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $uid = $basecamp_config['uid'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '.json';
  $options = _basecamp_http_options();
  $project = array();
  $project_json = drupal_http_request($basecamp_url, $options);
  $project = json_decode($project_json->data);
  if (empty($project)) {
    return FALSE;
  }
  return $project;
}

/**
 * Gets list of selected projects.
 * @return array containing the ids of selected projects.
 */
function _basecamp_selected_projects() {
  $all_projects = variable_get('basecamp_selected_projects', array());
  $selected_projects = array();
  foreach ($all_projects as $project) {
    if ($project != 0) {
      $selected_projects[] = $project;
    }
  }
  if (empty($selected_projects)) {
    return FALSE;
  }
  return $selected_projects;
}

/**
 * Gets names of selected projects.
 * @return array containing the names of selected projects.
 */
function _basecamp_get_selected_projects_names() {
  $basecamp_projects = _basecamp_selected_projects();
  if (empty($basecamp_projects)) {
    return FALSE;
  }
  foreach ($basecamp_projects as $project_id) {
    $project = _basecamp_get_project_by_id($project_id);
    $selected_projects[$project->name] = $project->name;
  }
  asort($selected_projects);
  return $selected_projects;
}

/**
 * Gets todolist ids.
 * @param $project_id.
 *   Id of the project.
 * @return string containing list of todolist ids.
 */
function _basecamp_todolist_ids($project_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $uid = $basecamp_config['uid'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $options = _basecamp_http_options();
  $todolist_url ='https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todolists.json';
  $todolist_json = drupal_http_request($todolist_url, $options);
  $todolists = json_decode($todolist_json->data);
  if (empty($todolists)) {
    return FALSE;
  }
  $todolist_ids = array();
  foreach ($todolists as $todolist) {
    $todolist_ids[] = $todolist->id;
  }
  return $todolist_ids;
}

/**
 * Function to get todolist ids and names.
 * @param $project_id.
 *   Id of the project.
 * @return string containing list of todolist ids and names.
 */
function _basecamp_todolists($project_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $uid = $basecamp_config['uid'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $options = _basecamp_http_options();
  $todolist_url ='https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todolists.json';
  $todolist_json = drupal_http_request($todolist_url, $options);
  $todolists = json_decode($todolist_json->data);
  if (empty($todolists)) {
    return FALSE;
  }
  $todolist_array = array();
  foreach ($todolists as $todolist) {
    $todolist_array[$todolist->id] = $todolist->name;
  }
  return $todolist_array;
}

/**
 * Function to get the list of todos for single project.
 * @param $project_id
 *   Id of project.
 * @param $todolist_ids
 *   Id of todolist.
 * @param $type
 *   Type of todo list.
 * @return array containing list of todos.
 */
function _basecamp_get_todos($project_id, $todolist_id, $type = NULL) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $uid = $basecamp_config['uid'];
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todolists/' . $todolist_id .'.json';
  $options = _basecamp_http_options();
  $todo_json = drupal_http_request($basecamp_url, $options);
  $todolist = json_decode($todo_json->data);
  if (empty($todolist)) {
    return FALSE;
  }
  $todos = $todolist->todos->remaining;
  switch ($type) {
    case 'ALL':
      $all_todos = array();
      foreach ($todos as $todo_item) {
        $all_todos[$todo_item->id] = $todo_item->content;
      }
      return $all_todos;
    case 'UNASSIGNED':
      $todo_unassigned = array();
      foreach ($todos as $todo) {
        if (!isset($todo->assignee)) {
          $todo_unassigned[] = $todo;
        }
      }
      $unassigned_todos = array();
      foreach ($todo_unassigned as $todo_item) {
        $unassigned_todos[$todo_item->id] = $todo_item->content;
      }
      return $unassigned_todos;
  }
}

/**
 * Function to get accesses of project.
 * @param $project_id
 *   Id of the project.
 * @return array of accesses.
 */
function _basecamp_get_accesses($project_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $uid = $basecamp_config['uid'];
  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/accesses.json';
  $options = _basecamp_http_options();
  $accesses_json = drupal_http_request($basecamp_url, $options);
  $accesses = json_decode($accesses_json->data);
  return $accesses;
}

/**
 * Function to get person.
 * @param $person_id
 *   Id of person.
 * @return array contatining information of person.
 */
function _basecamp_get_person($person_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $uid = $basecamp_config['uid'];
  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/people/' . $person_id . '.json';
  $options = _basecamp_http_options();
  $person_json = drupal_http_request($basecamp_url, $options);
  $person = json_decode($person_json->data);
  return $person;
}

/**
 * Function to create todo.
 * @param $project_id Id of project.
 * @param $todolist_id Id of todolist.
 * @param $todo_data Todo in json format.
 *
 * @return Id of todo created.
 */
function _basecamp_create_todo($project_id, $todolist_id, $todo_data) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $uid = $basecamp_config['uid'];

  $headers = array('Content-Type' => 'application/json');

  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todolists/' . $todolist_id .'/todos.json';

  $todo_status = drupal_http_request($basecamp_url, $headers, 'POST', $todo_data);
  $recent_todo = json_decode($todo_status->data);

  if ($todo_status->error == 'Created') {
    $a_options = array(
      'attributes' => array(
        'target' => '_blank',
      ),
    );
    drupal_set_message(t('Your todo has been created. Click ' . l('here', 'https://basecamp.com/' . $uid . '/projects/' . $project_id . '/todos/' . $recent_todo->id, $a_options) . ' to check your todo.'));
    return $recent_todo->id;
  }
  return FALSE;
}

/**
 * Function to update todo.
 * @param $project_id Id of project.
 * @param $todo_id Id of todo.
 * @param $todo_data Todo in json format.
 */
function _basecamp_update_todo($project_id, $todo_id, $todo_data) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $uid = $basecamp_config['uid'];

  $headers = array('Content-Type' => 'application/json');

  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todos/' . $todo_id . '.json';

  $todo_status = drupal_http_request($basecamp_url, $headers, 'PUT', $todo_data);
  if ($todo_status->code == 200) {
    $a_options = array(
      'attributes' => array(
        'target' => '_blank',
      ),
    );
    drupal_set_message(t('Your todo has been updated. Click ' . l('here', 'https://basecamp.com/' . $uid . '/projects/' . $project_id . '/todos/' . $recent_todo->id, $a_options) . ' to check your todo.'));
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Function to delete todo.
 * @param $project_id id of project.
 * @param @todo_id id of todo.
 */
function _basecamp_delete_todo($project_id, $todo_id) {
  $basecamp_config = variable_get('basecamp_configuration', array());
  $username = $basecamp_config['username'];
  $password = $basecamp_config['password'];
  $uid = $basecamp_config['uid'];

  $basecamp_url = 'https://' . $username . ':' . $password . '@www.basecamp.com/' . $uid . '/api/v1/projects/' . $project_id . '/todos/' . $todo_id . '.json';

  $todo_status = drupal_http_request($basecamp_url, array(), 'DELETE', $todo_data);
  drupal_set_message(t('Your todo has been deleted from basecamp.'));
}
