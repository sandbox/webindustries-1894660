$(document).ready(function() {

 $("#edit-basecamp-projects").change(function(){
  var pid = $('#edit-basecamp-projects').val();
  $('#edit-basecamp-projects').after('<div class="members-data-refresh" style="background-color: yellow;display:block;width:250px">Wait.. Loading Todos list and Members</div>');
  $.ajax({
		 type: "POST",
		 url: "/basecamp/get_project_details",
		 data: {basecamp_pid :pid},
     dataType: "json",
     success: function (data) {
      var newTodosOptions = data.data.todos;
			var $todos = $("#edit-basecamp-todo-list");
			$todos.empty(); // remove old options
			$.each(newTodosOptions, function(key, value) {
				$todos.append($("<option></option>")
					 .attr("value", key).text(value));
			});

      var newMembersOptions = data.data.members;
			var $members = $("#edit-basecamp-people-list");
			$members.empty(); // remove old options
			$.each(newMembersOptions, function(key, value) {
			$members.append($("<option></option>")
				.attr("value", key).text(value));
			});
     $('.members-data-refresh').css('display','none');
     },
	 });
  });
});
